<?php

// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/register', function () {
//     return view('register');
// });

Route::get('/','App\Http\Controllers\HomeController@home');
Route::get('/register','App\Http\Controllers\AuthController@register');
Route::post('/welcomesanber','App\Http\Controllers\AuthController@tangkap');
?>