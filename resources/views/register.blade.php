<!DOCTYPE html>
<html>
    <head>
        <title>Form SignUp</title>
    </head>
    <body>

    <form action="/welcomesanber" method = "post">
    @csrf
        <h1>Buat Account baru!</h1>
        <h3>Sign Up Form</h3>
        
            <label>First name :</label><br><br>
            <input type="text" name="fnama"/><br><br>
            <label>Last name :</label><br><br>
            <input type="text" name="lnama"/><br><br>
            <label>Gender :</label><br><br>
            <label><input type="radio" name="gender" value="Male"/>Male</label><br>
            <label><input type="radio" name="gender" value="Female"/>Female</label><br>
            <label><input type="radio" name="gender" value="Other"/>Other</label><br>
            <br>
            <label>Nationality :</label><br><br>
            <select name="Nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="British">British</option>
                <option value="Germany">Germany</option>
            </select>
            <br><br>
            <label>Language Spoken :</label><br><br>
            <label><input type="checkbox" name="language" value="Bahasa Indonesia"/>Bahasa Indonesia</label><br>
            <label><input type="checkbox" name="language" value="English"/>English</label><br>
            <label><input type="checkbox" name="language" value="Other"/>Other</label><br>
            <br>
            <label>Bio :</label><br><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br>
            <br>
            <input type="submit" value="Sign Up"/>
        </form>
    </body>
</html>